package com.company.hiro.epistemic;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class SplashActivity extends AppCompatActivity {

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        GlobalFactor.mAuth = FirebaseAuth.getInstance();
        if(GlobalFactor.mAuth.getCurrentUser() != null){
            ReadUser();
        }else{
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ToLoginActivity();
                }
            }, 3000);
        }

    }
    void ReadUser(){
        DatabaseReference referenceEmailLoginReference = GlobalFactor.usersRef.
                child(GlobalFactor.mAuth.getUid());
        referenceEmailLoginReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GlobalFactor.user = dataSnapshot.getValue(User.class);
                if (GlobalFactor.user.getName() !=null &&  !GlobalFactor.user.getName().isEmpty()) {
                    ToMainActivity();
                }else {
                    GlobalFactor.mAuth.signOut();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("Error", error.getMessage());
            }
        });
    }

    void ToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
    void ToLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
