package com.company.hiro.epistemic;


import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SettingsFragment extends Fragment {

    View view;
    Spinner notificationPerDaySpn;
    Button save;

    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings, container, false);
        context = getContext();

        GlobalFactor.setBackArrowOn();

        //region setar variaveis
        notificationPerDaySpn = view.findViewById(R.id.notificationPerDaySpn);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                R.array.notificationPerDay, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        notificationPerDaySpn.setAdapter(adapter);
        //endregion

        //region função do botão
        save = view.findViewById(R.id.saveBtn);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int perDay = notificationPerDaySpn.getSelectedItemPosition() + 1;
                GlobalFactor.user.
                        setNotificationsFrequenci(perDay);
                toggle_notifications();
                GlobalFactor.fragmentManager.popBackStack();
            }
        });
        //endregion

        return view;
    }

    Date now;
    public void toggle_notifications() {
        int current_hour, notify_per_day, midnights = 0;
        now = new Date();
        List<Integer> notify_times = new ArrayList<>();
        notify_per_day = notificationPerDaySpn.getSelectedItemPosition() + 1;
        SimpleDateFormat dateFormatter = new SimpleDateFormat("hh");
        current_hour = Integer.valueOf(dateFormatter.format(now));

        int timeSkip = 0;

        if(notify_per_day > 0){
            int div = 24/(notify_per_day);
            for (int i = 0; i < notify_per_day; i++){
                current_hour += div;

                if (current_hour >= 24){
                    current_hour -= 24;
                    midnights ++;
                }

                if(timeSkip < (midnights * 7)){
                    if (current_hour >= 0 && current_hour <= 6){
                        timeSkip = 7 - current_hour;
                    }
                }
                int hourAdd = current_hour + timeSkip;
                notify_times.add(hourAdd);
                NotificationBuilder(hourAdd, midnights);
            }
        }else{
            notify_times.clear();
        }
        GlobalFactor.user.setNotifyTimes(notify_times);
    }
    void NotificationBuilder(int hour, int midnight){
        long when = now.getTime();

        SimpleDateFormat format1 = new SimpleDateFormat("hh");
        int hournow = Integer.parseInt(format1.format(now));

        when += (midnight * 86400000);

        int diference = hour - hournow;

        when += diference * 3600000;

        Date date = new Date();
        date.setTime(when);
        SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy HH");
        String dateLog = fmtOut.format(date);
        Log.i("Hour Notification", dateLog);

        NotificationManager notificationManager = (NotificationManager) GlobalFactor.mainActivity
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(GlobalFactor.mainActivity, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(
                context, "M_CH_ID").setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Notificação Prova App")
                .setContentText("Notificação Marcada para " + dateLog)
                .setWhen(when)
                .setContentIntent(pendingIntent);
        int notificationID = 0;
        notificationManager.notify(notificationID, mNotifyBuilder.build());
    }
}
