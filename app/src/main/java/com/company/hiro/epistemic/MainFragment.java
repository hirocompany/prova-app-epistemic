package com.company.hiro.epistemic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class MainFragment extends Fragment {


    //region variaveis
    View view;
    TextView nameTxt;
    TextView nextnotificationTxt;
    //endregion

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);

        nameTxt = view.findViewById(R.id.userNameTxt);
        nameTxt.setText(getString(R.string.hi) + " " + GlobalFactor.user.getName());

        nextnotificationTxt = view.findViewById(R.id.nextNotification);

        return view;
    }

    @Override
    public void onResume() {

        GlobalFactor.setBackArrowOff();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                GlobalFactor.usersRef.child(GlobalFactor.mAuth.getUid()).child("notificationsFrequenci").setValue(GlobalFactor.user.getNotificationsFrequenci());
//                GlobalFactor.usersRef.child(GlobalFactor.mAuth.getUid()).child("notifyTimes").setValue(GlobalFactor.user.getNotifyTimes());
            }
        });
        thread.run();

//        try{
//            GlobalFactor.usersRef.child(GlobalFactor.user.getId()).setValue(GlobalFactor.user);
//        }catch (Exception e){
//            Log.e("Error", e.toString());
//        }

        //region texto das notifications
        String text;
        List<Integer> notificationTimes = GlobalFactor.user.getNotifyTimes();
        if(notificationTimes != null){
            text = "Você escolheu " + GlobalFactor.user.getNotificationsFrequenci() + " de notificações.\nAs notificações serão as: \n";
            for (int i = 0; i < notificationTimes.size(); i++){
                if(i == notificationTimes.size() - 1)
                    text += notificationTimes.get(i);
                else if(i == notificationTimes.size() - 2)
                    text += notificationTimes.get(i) + " e ";
                else
                    text += notificationTimes.get(i) + ", ";
            }
            text += ".";
        }
        else
            text = "Não há notificações programadas.";
        nextnotificationTxt.setText(text);
        //endregion

        super.onResume();
    }
}
