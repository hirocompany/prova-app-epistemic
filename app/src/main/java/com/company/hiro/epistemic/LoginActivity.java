package com.company.hiro.epistemic;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.util.JsonUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class LoginActivity extends AppCompatActivity{

    //region variaveis
    EditText emailEdt;
    EditText passwordEdt;

    TextView signupTxt;
    TextView passwordRecoverTxt;
    TextView termOfUseTxt;

    Button loginBtn;

    ProgressBar  progressBar;
    long loginTry;

    AppCompatActivity appCompatActivity;

    Date currentTime;
    long timeToAcesseAgain;

    String emailLoginConverted;
    boolean stop;

    SharedPreferences sharedPreferences;
    String preference = "SharedePreference";

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        appCompatActivity = this;

        //region variable settings
        emailEdt = findViewById(R.id.EmailEdt);
        passwordEdt = findViewById(R.id.PasswordEdt);

        progressBar = findViewById(R.id.progress_bar);

        signupTxt = findViewById(R.id.SignUpTxt);
        signupTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUp();
            }
        });

        passwordRecoverTxt = findViewById(R.id.PasswordRecoverTxt);
        passwordRecoverTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertRecoveryPopUp();
            }
        });

        termOfUseTxt = findViewById(R.id.TermOfUseTxt);
        termOfUseTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenThermOfUse();
            }
        });

        loginBtn = findViewById(R.id.LoginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    String email = emailEdt.getText().toString();
                    String password = passwordEdt.getText().toString();
                    if(!email.isEmpty() && !password.isEmpty()){
                        emailLoginConverted = email.replace(".", "!");
                        if(LoginTry())
                            Login(email, password);
                    }

            }
        });
        //endregion

        //region Firebase

        if(GlobalFactor.mAuth.getCurrentUser() != null){
            ReadUser();
        }
        //endregion
    }

    @Override
    protected void onStart() {
        super.onStart();
        GlobalFactor.currentUser = GlobalFactor.mAuth.getCurrentUser();

    }

    @Override
    protected void onResume() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        if(GlobalFactor.mAuth.getCurrentUser() != null)
        emailEdt.setText(GlobalFactor.currentUser.getEmail());
        super.onResume();
    }

    //region metodo login
    void Login(final String email, String password){

        progressBar.setVisibility(View.VISIBLE);

        GlobalFactor.mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        ///progressbar off
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            //region zerar logintry
                            loginTry = 0;
                            GlobalFactor.emailFailedRef.
                                    child(emailLoginConverted).
                                    child(GlobalFactor.refEmailTry_trys).setValue(loginTry);
                            //endregion
                            ReadUser();
                        } else {
                            String message;
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthInvalidCredentialsException e) {
                                loginTry ++;
                                GlobalFactor.emailFailedRef.child(emailLoginConverted).child(GlobalFactor.refEmailTry_trys).setValue(loginTry);
                                String messangeLoginTry = getString(R.string.email_or_password_incorrect) +
                                        "(" + loginTry + ")";
                                message = messangeLoginTry;
                            }catch (FirebaseAuthInvalidUserException e){
                                message = getString(R.string.email_not_registred);
                            }catch(Exception e) {
                                message = e.toString();
                            }
                            GlobalFactor.SimpleAlert(appCompatActivity,message);
                        }
                    }
                });
    }
    void ReadUser(){
        progressBar.setVisibility(View.VISIBLE);
        DatabaseReference referenceEmailLoginReference = GlobalFactor.usersRef.
                child(GlobalFactor.mAuth.getUid());
        referenceEmailLoginReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                progressBar.setVisibility(View.GONE);
                GlobalFactor.user = dataSnapshot.getValue(User.class);
                if (GlobalFactor.user.getName() !=null &&  !GlobalFactor.user.getName().isEmpty()) {
                    ToMainActivity();
                }else {
                    GlobalFactor.mAuth.signOut();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.e("Error", error.getMessage());
            }
        });
    }
    boolean LoginTry(){
        currentTime = Calendar.getInstance().getTime();
        stop = false;
        final DatabaseReference referenceEmailLoginConverted = GlobalFactor.emailFailedRef.
                child(emailLoginConverted);
        referenceEmailLoginConverted.
                child(GlobalFactor.refEmailTry_time).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                   timeToAcesseAgain = dataSnapshot.getValue(long.class);
                   long currenteTimeLong = currentTime.getTime();
                   if(currenteTimeLong < timeToAcesseAgain){
                       String dataString = DateFormat.format("dd/MM/yyyy HH:mm", new Date(timeToAcesseAgain)).toString();
                       GlobalFactor.SimpleAlert(appCompatActivity,"Bloqueio temporário para a conta " + emailEdt.getText().toString() +
                               "\nTente acessar novamente em " + dataString);
                       stop = true;
                   }
                   else{
                       loginTry = 0;
                       referenceEmailLoginConverted.
                               child(GlobalFactor.refEmailTry_trys).setValue(loginTry);
                   }
                }catch (Exception e){
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        if (stop)
            return false;
        else
            referenceEmailLoginConverted.child(GlobalFactor.refEmailTry_trys).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(long.class) == null) {
                    loginTry = 0;
                }
                else
                    loginTry = dataSnapshot.getValue(long.class);
            }

            @Override
            public void onCancelled(DatabaseError error) {
            }
        });
        if (loginTry < 3)
            return true;
        else{
            long timeToNextAcess = currentTime.getTime();
            timeToNextAcess += 60000 * GlobalFactor.userMinuteBlocked;
            referenceEmailLoginConverted.child(GlobalFactor.refEmailTry_time).setValue(timeToNextAcess);
            return false;
        }
    }
    void ToMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    //endregion

    void SignUp(){
        Intent myIntent = new Intent(this, SignUpActivity.class);
        startActivity(myIntent);
    }

    //region Hiperlink recuperar senha
    void AlertRecoveryPopUp(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.reset_password);
        alert.setMessage(R.string.your_email);

        final EditText emailRecoveryEdt = new EditText(this);
        alert.setView(emailRecoveryEdt);
        emailRecoveryEdt.setText(emailEdt.getText().toString());
        alert.setPositiveButton(R.string.reset_password, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                progressBar.setVisibility(View.VISIBLE);
                RecoveyPassword(emailRecoveryEdt.getText().toString());
            }
        });
        alert.setNegativeButton(R.string.cancel, null);
        alert.show();
    }
    void RecoveyPassword(String email){
        FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            GlobalFactor.SimpleAlert(appCompatActivity,"Link para recuperar a senha enviado para o seu e-mail");
                        }
                        else{
                            GlobalFactor.SimpleAlert(appCompatActivity,"Aviso", "E-mail não cadastrado");
                        }
                    }
                });
    }
    //endregion

    void OpenThermOfUse(){
        GlobalFactor.SimpleAlert(appCompatActivity,getResources().getString(R.string.term_of_use), getString(R.string.lorem_ipsum));
    }

}
