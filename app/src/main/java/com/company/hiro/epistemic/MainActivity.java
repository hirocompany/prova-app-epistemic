package com.company.hiro.epistemic;

import android.content.DialogInterface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    FrameLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GlobalFactor.mainActivity = this;

        MainFragment mainFragment = new MainFragment();
        GlobalFactor.replaceFragment(this, mainFragment);

    }

    @Override
    public void onBackPressed() {
        if(GlobalFactor.fragmentManager.getBackStackEntryCount() > 1){
            GlobalFactor.fragmentManager.popBackStack();
        }else{
            AlertExitApp();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.configurations, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                SettingsFragment settingsFragment = new SettingsFragment();
                GlobalFactor.replaceFragment(this, settingsFragment);
                return true;
            case android.R.id.home:
                GlobalFactor.fragmentManager.popBackStack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void AlertExitApp(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Sair do aplicativo?");
        alert.setMessage("Deseja sair do aplicativo?");
        alert.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                GlobalFactor.mAuth.signOut();
                finish();
            }
        });
        alert.setNegativeButton("Não", null);
        alert.show();
    }
}
