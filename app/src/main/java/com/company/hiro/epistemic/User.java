package com.company.hiro.epistemic;

import java.util.List;

class User {

    private String name;
    private String email;
    private String id;
    private int notificationsFrequenci = 0;
    private List<Integer> notifyTimes;

    //region get set
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNotificationsFrequenci() {
        return notificationsFrequenci;
    }

    public void setNotificationsFrequenci(int notificationsFrequenci) {
        this.notificationsFrequenci = notificationsFrequenci;
    }

    public List<Integer> getNotifyTimes() {
        return notifyTimes;
    }

    public void setNotifyTimes(List<Integer> notifyTimes) {
        this.notifyTimes = notifyTimes;
    }

    //endregion

}
