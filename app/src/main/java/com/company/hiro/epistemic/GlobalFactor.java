package com.company.hiro.epistemic;

import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

class GlobalFactor {

    public static MainActivity mainActivity;
    public static void setBackArrowOn(){
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    public static void setBackArrowOff(){
        mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    //region Firebase
    public static FirebaseAuth mAuth;
    public static FirebaseUser currentUser;
    public static FirebaseDatabase database = FirebaseDatabase.getInstance();
    //endregion

    //region ref
    public static String dataRefUser = "users";
    public static String dataRefEmailTry = "emailtry";
    //endregion

    public static String refEmailTry_trys = "trys";
    public static String refEmailTry_time = "time";
    public static int userMinuteBlocked = 5;

    public static DatabaseReference usersRef = database.getReference(dataRefUser);
    public static DatabaseReference emailFailedRef = database.getReference(dataRefEmailTry);

    public static FragmentManager fragmentManager;
    public static FragmentTransaction fragmentTransaction;

    public static User user;

    public static void replaceFragment(FragmentActivity fragmentActivity, Fragment frag) {
        fragmentManager = fragmentActivity.getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container,frag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static boolean ValidEmail (String email){
        String[] stringsEmail = email.split("@");
        if (stringsEmail.length == 2)
            return true;
        return false;
    }

    //region simple alert
    public static void SimpleAlert(AppCompatActivity appCompatActivity, String message){
        AlertDialog.Builder alert = new AlertDialog.Builder(appCompatActivity);
        alert.setTitle("");
        alert.setMessage(message);
        alert.setNegativeButton(R.string.close,null);
        alert.show();
    }
    public static void SimpleAlert(AppCompatActivity appCompatActivity, String title, String message){
        AlertDialog.Builder alert = new AlertDialog.Builder(appCompatActivity);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setNegativeButton(R.string.close,null);
        alert.show();
    }
    public static void SimpleAlert(AppCompatActivity appCompatActivity, String title, String message, DialogInterface.OnClickListener positiveButton){
        AlertDialog.Builder alert = new AlertDialog.Builder(appCompatActivity);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton(R.string.ok, positiveButton);
        alert.show();
    }
    public static void SimpleAlert(AppCompatActivity appCompatActivity, String title, String message, DialogInterface.OnClickListener positiveButton, DialogInterface.OnClickListener negativeButton){
        AlertDialog.Builder alert = new AlertDialog.Builder(appCompatActivity);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton(R.string.ok, positiveButton);
        alert.setNegativeButton(R.string.close,negativeButton);
        alert.show();
    }
        //endregion

}
