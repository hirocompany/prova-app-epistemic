package com.company.hiro.epistemic;


import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;


public class SignUpActivity extends AppCompatActivity {

    //region variaveis
    EditText emailEdt;
    EditText nameEdt;
    EditText passwordEdt;
    EditText passwordRepeatEdt;

    Button signinBtn;

    ProgressBar progressBar;

    AppCompatActivity appCompatActivity;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        appCompatActivity = this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //region set variable
        emailEdt = findViewById(R.id.EmailEdt);
        nameEdt = findViewById(R.id.NameEdt);
        passwordEdt = findViewById(R.id.PasswordEdt);
        passwordRepeatEdt = findViewById(R.id.PasswordRepeatEdt);

        progressBar = findViewById(R.id.progress_bar);

        signinBtn = findViewById(R.id.SignUpBtn);
        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailEdt.getText().toString();
                String password = passwordEdt.getText().toString();
                if (GlobalFactor.ValidEmail(email)){
                    if(password.length() >= 6){
                        if(password.equals(passwordRepeatEdt.getText().toString())){
                            ///region progressbar on
                            progressBar.setVisibility(View.VISIBLE);
                            MakeSignup(email, password);
                        }
                        else{
                            GlobalFactor.SimpleAlert(appCompatActivity, "Senhas não conferem");
                        }
                    }
                    else{
                        GlobalFactor.SimpleAlert(appCompatActivity,"Senha muito fraca.\nUtilize senha com 6 ou mais caracteres.");
                    }
                }
            }
        });
        //endregion
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void MakeSignup(final String email, String password){
        GlobalFactor.mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        ///progressbar off
                        progressBar.setVisibility(View.GONE);

                        if (task.isSuccessful()) {
                            String emailFirebase = email.replace(".","!");

                            GlobalFactor.currentUser = GlobalFactor.mAuth.getCurrentUser();

                            GlobalFactor.currentUser.sendEmailVerification()
                                    .addOnCompleteListener(appCompatActivity, new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                };
                                                GlobalFactor.SimpleAlert(appCompatActivity
                                                        ,"Conta cadastrada com sucesso"
                                                        ,"Link para confirmação de cadastro enviado para o e-mail " + GlobalFactor.currentUser.getEmail()
                                                        , onClickListener);
                                            }else{
                                                GlobalFactor.SimpleAlert(appCompatActivity,
                                                        "Erro ao enviar e-mail de confirmação de cadastro.");
                                            }
                                        }
                                    });

                            User newUser = new User();
                            newUser.setEmail(email);
                            newUser.setName(nameEdt.getText().toString());
                            newUser.setId(GlobalFactor.currentUser.getUid());

                            GlobalFactor.usersRef.child(GlobalFactor.currentUser.getUid()).setValue(newUser);
                            GlobalFactor.emailFailedRef.child(emailFirebase);
                        } else {
                            String messange;
                            try {
                                throw task.getException();
                            } catch(FirebaseAuthWeakPasswordException e) {
                                messange = "Senha muito fraca.\nUtilize senha com 6 ou mais caracteres.";
                            } catch(FirebaseAuthUserCollisionException e) {
                                messange = "E-mail já cadastrado";
                            } catch(Exception e) {
                                messange = e.getMessage();
                            }
                            GlobalFactor.SimpleAlert(appCompatActivity, "Error ao realizar o Cadastro." , messange);
                        }
                    }
                });
    }
}
